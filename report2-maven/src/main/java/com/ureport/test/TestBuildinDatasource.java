package com.ureport.test;

import com.bstek.ureport.definition.datasource.BuildinDatasource;
import org.springframework.beans.factory.annotation.Autowired;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author 周宇
 * @create 2020-12-13 22:12
 */
public class TestBuildinDatasource implements BuildinDatasource {

    private DataSource dataSource;

    @Override
    public String name() {
        return "内置数据源DEMO";
    }

    @Override
    public Connection getConnection() {
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
