package com.ureport.test;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 周宇
 * @create 2020-12-13 21:35
 */
@Data
@NoArgsConstructor
public class User {
    private int id;
    private String name;
    private int salary;
}
