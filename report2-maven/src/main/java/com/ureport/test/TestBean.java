package com.ureport.test;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 周宇
 * @create 2020-12-13 21:25
 * SpringBean方式数据源
 */
public class TestBean {

    public List<Map<String,Object>> loadReportData(String dsName, String datasetName, Map<String,Object> parameters){
        List<Map<String,Object>> list = new ArrayList<>();
        for (int i =0; i<1000; i++){
            Map<String,Object> map = new HashMap<>();
            map.put("id",i);
            map.put("name", RandomStringUtils.random(10,true,false));
            map.put("salary", RandomUtils.nextInt(10000)+1);
            list.add(map);
        }
        return list;
    }

    public List<User> buildReport(String dsName,String datasetName,Map<String,Object> parameters){
        List<User> list = new ArrayList<>();
        for (int i =0; i<1000; i++){
            User user = new User();
            user.setId(i);
            user.setName( RandomStringUtils.random(10,true,false));
            user.setSalary(RandomUtils.nextInt(10000)+1);
            list.add(user);
        }
        return list;
    }

}
